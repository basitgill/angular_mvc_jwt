﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Angular_mvc_jwt.App_Start;
namespace Angular_mvc_jwt
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}